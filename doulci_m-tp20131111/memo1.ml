let int_sqrt n =
    let h = Hashtbl.create 42 in
    let r = ref n in
    let r' = ref (n+1) in
    try
        Hashtbl.find h n;
    with
    | Not_found -> 
            while !r' > !r do
                r' := !r;
                r := (!r + n/(!r))/2;
            done;
            Hashtbl.add h n !r';
            !r'

let rec fibo count x =
    let h = Hashtbl.create 42 in
    try
        Hashtbl.find h x;
    with
    | Not_found ->
            let rec aux = function
                | (0|1) as x -> x
                | n -> incr count; 
                Hashtbl.add h (n-1) (fibo count (n-1));
                Hashtbl.add h (n-2) (fibo count (n-2));
                (Hashtbl.find h (n-1)) + (Hashtbl.find h (n-2))
            in aux x
