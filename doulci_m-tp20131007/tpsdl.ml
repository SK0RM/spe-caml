(* Dimensions d'une image *)
let get_dims img =
  ((Sdlvideo.surface_info img).Sdlvideo.w, (Sdlvideo.surface_info img).Sdlvideo.h)
 
(* init de SDL *)
let sdl_init () =
  begin
    Sdl.init [`EVERYTHING];
    Sdlevent.enable_events Sdlevent.all_events_mask;
  end
 
(* attendre une touche ... *)
let rec wait_key () =
  let e = Sdlevent.wait_event () in
    match e with
    Sdlevent.KEYDOWN _ -> ()
      | _ -> wait_key ()
 
(*
  show img dst
 affiche la surface img sur la surface de destination dst (normalement l'écran)
*)
let show img dst =
  let d = Sdlvideo.display_format img in
    Sdlvideo.blit_surface d dst ();
    Sdlvideo.flip dst

(* My Functions *)
let foi x = float_of_int x
let iof x = int_of_float x

let level (r, g, b) = (0.3 *. (foi r) +. 0.59 *. (foi g) +. 0.11 *. (foi b)) /.
255.

let color2grey (r, g, b) = 
    let grey = iof(255. *. level (r, g, b)) in (grey, grey, grey)

let image2grey imgA imgB =
    let (w, h) = get_dims(imgA) in
    for i = 0 to w do
        for j = 0 to h do
            Sdlvideo.put_pixel_color (imgB:Sdlvideo.surface) i j 
            (color2grey(Sdlvideo.get_pixel_color imgA i j))
        done
    done

let make_out (source) = match get_dims(source) with
   | (width, height) -> 
           Sdlvideo.create_RGB_surface_format source [] width height 

let print_dim (img) = match get_dims(img) with
    | (w, h) -> print_string("(" ^ string_of_int w ^ ";" ^ string_of_int h ^ ")")
(****************) 

(* main *)
let main () =
  begin
    (* Nous voulons 1 argument *)
    if Array.length (Sys.argv) < 2 then
      failwith "Il manque le nom du fichier!";
    (* Initialisation de SDL *)
    sdl_init ();
    (* Chargement d'une image *)
    let img = Sdlloader.load_image Sys.argv.(1) in
    (* On récupère les dimensions *)
    let (w,h) = get_dims img in
    (* On crée la surface d'affichage en doublebuffering *)
    let display = Sdlvideo.set_video_mode w h [`DOUBLEBUF] in
      (* on affiche l'image *)
      show img display;
      (* on attend une touche *)
      wait_key ();
      (*On creer une new img*)
      let out = make_out (img) in
      (*niveau de gris*)
      image2grey img out;
      (*affiche result*)
      show out display;
      (*on attend une touche*)
      wait_key ();
      (* on quitte *)
      exit 0
  end
 
let _ = main ()
