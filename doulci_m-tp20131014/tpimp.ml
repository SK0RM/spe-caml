let next_fact =
    let a = ref 1 and i = ref 1 in 
    fun () -> a := !a * !i; incr i; !a

let build_next_seg u0 f =  
    let u0_ = ref u0 and n = ref 1 in
    let next () =
    u0_ := f !n !u0_ ; incr n ; !u0_ in
    next

let main () = 
    begin
        for i = 0 to 10 do
            print_int(next_fact ());
            print_newline();
        done;
        let fact n u = n * u in
        let next_fact2 = build_next_seg 1 fact in
        for i = 0 to 10 do
            print_int(next_fact2 ());
            print_newline();
        done;
    end

let _ = main ()
