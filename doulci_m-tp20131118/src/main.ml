(* Testing Heap *)
 
let h = Heap.create 256 neg_infinity ""

(* Build a hash table with (k,v) to be inserted (and insert them) *)
(* Store minimal key for later test *)
let minkey = ref infinity
let value_minkey = ref ""
let data =
  let _ = Random.init 42 in
  let _h = Hashtbl.create 521 in
    for i = 1 to 33 do
      let f = float (Random.int 4096) in
        minkey := min !minkey f;
        if !minkey = f then
          value_minkey := string_of_int i;
        Hashtbl.add _h (string_of_int i) f;
        Printf.eprintf "Adding: (%g,%S);" f (string_of_int i);
        if (i mod 4 = 0) then Printf.eprintf "\n%!";
        Heap.insert h f (string_of_int i);
    done;
    Printf.eprintf "\n%!";
    _h

(* Our main test *)

let main () =
  begin
    Printf.printf "take_min: %S (should be %S)\n%!"
      (Heap.take_min h) !value_minkey;
    Printf.printf "Update all reaming keys\n%!";
    Hashtbl.iter (
      fun v k ->
        Heap.update h v (float(Random.int 4096))
    ) data;
    Printf.printf "Getting all keys\n%!";
    while (not (Heap.is_empty h)) do
      Printf.printf "value: %S\n%!" (Heap.take_min h);
    done;
    Printf.printf "done\n%!";
    exit 0;
  end

let _ =
  try
    Printexc.record_backtrace true;
    main ();
  with
    | e ->
        Printf.eprintf "%s\n%!" (Printexc.to_string e);
        Printexc.print_backtrace stderr;
        exit 42
