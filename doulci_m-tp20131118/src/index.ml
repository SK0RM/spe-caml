(* Module Index: index.ml *)
(* index for heap *)
 
type value = string
                
type t = (value,int) Hashtbl.t
            
let create capacity =
    Hashtbl.create capacity
       
let update k pos index =
      Hashtbl.replace index k pos
         
let remove k index =
    Hashtbl.remove index k
       
let get k index =
    Hashtbl.find index k
