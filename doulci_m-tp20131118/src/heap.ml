(* Heap implementation *)
 
exception Capacity_exceeded
   
type key = float
type value = string
                
type cell = {
  key : key;
  value : value;
}
               
type t = {
  tab : cell array;
  index : Index.t;
  mutable size : int;
  capacity : int;
}
            
let is_empty h = h.size = 0
                             
let create capacity min_key dumm_val =
  {
    tab = Array.make (capacity + 1) {key=min_key;value=dumm_val};
    index = Index.create (capacity+1);
    capacity = capacity;
    size = 0;
  }
       
let insert h k v =
  h.size <- h.size + 1;
  let n = { key = k; value = v } in
    h.tab.(h.size) <- n;
    Hashtbl.add h.index v h.size;
    let i = ref h.size in
      while (h.tab.(!i/2).key > h.tab.(!i).key) do
        let p = h.tab.(!i/2) in
          h.tab.(!i/2) <- n;
          Index.update (n.value) (!i/2) (h.index);
          h.tab.(!i) <- p;
          Index.update (p.value) (!i) (h.index);
          i := !i / 2;
      done
                      
let take_min h =
  let l = h.tab.(h.size) and v = h.tab.(1).value in 
    h.tab.(1) <- l;
    Index.update (l.value) (1) (h.index);
    
    h.size <- h.size - 1;
    let i = ref 1 and j = ref 0 in
      while (((h.tab.(!i).key > h.tab.(2 * !i).key) || 
              (h.tab.(!i).key > h.tab.(2 * !i + 1).key)) && 
             (!i <= h.size) && (!j <= h.size)) do

        if (h.tab.(2 * !i).key < h.tab.(2 * !i + 1).key) then 
          j := 2 * !i else j := 2 * !i + 1;

        if (!j <= h.size) then
          begin
        h.tab.(!i) <- h.tab.(!j);
        Index.update (h.tab.(!j).value) (!i) (h.index);
        h.tab.(!j) <- l;
        Index.update (l.value) (!j) (h.index);
        i := !j;
          end;

      done;
      v
                    
let update h v newkey =
  let i = ref (Index.get v (h.index)) in
    let old = h.tab.(!i) in
    let neu = { key = newkey ; value = old.value } in
      h.tab.(!i) <- neu;

    if (h.tab.(!i).key > h.tab.(!i/2).key) then
      begin
        
          let j = ref 0 in
            while (((h.tab.(!i).key > h.tab.(2 * !i).key) ||
                    (h.tab.(!i).key > h.tab.(2 * !i + 1).key)) &&
                   (!i <= h.size) && (!j <= h.size)) do
              if (h.tab.(2 * !i).key < h.tab.(2 * !i + 1).key) then
                j := 2 * !i else j := 2 * !i + 1;

              if (!j <= h.size) then
                begin
              h.tab.(!i) <- h.tab.(!j);
              Index.update (h.tab.(!j).value) (!i) (h.index);
              h.tab.(!j) <- neu;
              Index.update (neu.value) (!j) (h.index);
                end;
                
              i := !j;
            done;
        end else begin
          while (h.tab.(!i/2).key > h.tab.(!i).key) do
            h.tab.(!i) <- h.tab.(!i/2);
            Index.update (h.tab.(!i/2).value) (!i) h.index;
            h.tab.(!i/2) <- neu;
            Index.update (neu.value) (!i/2) h.index;
            
            i := !i / 2;
            done;
        end
