(* Heap implementation *)
 
(* Exception raised when heap is full *)
exception Capacity_exceeded
   
(* Heap content *)
type key = float
type value = string
type cell = {
  key : key;
  value : value;
}
               
(* Heap data structure *)
type t = {
  tab : cell array;
  index : Index.t;
  mutable size : int;
  capacity : int;
}
            
(* return true if the heap is empty, false otherwise *)
val is_empty : t -> bool
                       
(* create cap minkey dumm:
 * create a new of capacity cap with a sentinel {minkey,dumm}
 * *)
val create : int -> key -> value -> t
                                       
(* insert h k v
 * insert {k,v} into heap h
 * *)
val insert : t -> key -> value -> unit
                                     
(* take_min h
 * extract minimal value (updating the whole heap) and return it
 * *)
val take_min : t -> value
                       
(* update h value newkey
 * find pair {oldkey, value}, replace oldkey with newkey and update
 * the heap.
 * If the pair wasn't in the heap, update acts as insert.
 * *)
val update : t -> value -> key -> unit
