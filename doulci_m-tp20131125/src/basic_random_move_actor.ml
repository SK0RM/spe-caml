class ['a] basic_random_move_actor range_x range_y =
object
  inherit ['a] abstract_actor
  
  method treat e =
    let (x,y) = (Random.int range_x, Random.int range_y) in
      e#move_to x y
end

