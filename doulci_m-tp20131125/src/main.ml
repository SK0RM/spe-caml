let help () =
  Printf.printf "How to use living :\n";
  Printf.printf "living m w h l";
  Printf.printf "\nEnjoy !\n"

let main () =
  begin
    let mode = ref (-1) and w = ref (-1) and h = ref (-1) and l = ref (-1) in
      try
        mode := int_of_string(Sys.argv.(1));
        w := int_of_string(Sys.argv.(2));
        h := int_of_string(Sys.argv.(3));
        l := int_of_string(Sys.argv.(4));

        if (!mode = 0) then main_text(!w, !h, !l) else main_graph(!w, !h, !l)
      with
        | _ -> help()
  end

let _ = main()
