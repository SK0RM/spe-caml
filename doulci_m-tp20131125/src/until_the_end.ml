let rec until_the_end master =
    try
        master#next_turn;
        until_the_end master
    with
    The_End -> ()

