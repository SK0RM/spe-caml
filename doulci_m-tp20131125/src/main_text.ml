let main_text (w, h, l) =
    begin
        let (dX, dY) = (w, h) in
        let board = new iterable_board (new board dX dY) in
        let master = new text_master 10 board dX dY
        [("red",'R');("green", 'G');("blue",'B')] in
        master#register_actor (new random_move_actor dX dY l);
        master#init_board;
        master#display;
        until_the_end master;
        exit 0
    end

