class team board color attcoef =
object (self)
  val mutable members = 0
  val defcoef = 100 - attcoef

  (* Create a new entity *)
  method new_member x y =
    let d1 = Random.int 10
    and d2 = Random.int 11 in
    let base = d1 * 10 + d2 in
    let (str, def) = (attcoef * base, defcoef * base) in
    let name = Printf.sprintf "%s%02d" color members in
      members <- members + 1;
      new fight_entity name x y color board str def
end

