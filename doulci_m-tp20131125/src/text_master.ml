class ['a] text_master turns board size_x size_y colors =
object (self)
  inherit ['a] team_master turns board size_x size_y

  val tab = Hashtbl.create 11

  initializer
    List.iter (fun (col,ch) -> Hashtbl.add tab col ch) colors

  (* Get the char for a color using Hashtbl.find tab color *)
  method display =
    let line w =
      for i = 0 to (2 * w) do
        Printf.printf "-"
      done;
      Printf.printf "\n"
    in
    let row y =
      Printf.printf "|";
      for i = 0 to size_x - 1 do
        (
          try
            Printf.printf "%c" (Hashtbl.find tab (board#get_entity i y)#get_color);
          with
            | _ -> Printf.printf "X"
        );
        Printf.printf "|"
      done;
      Printf.printf "\n"
    in
      for y = 0 to size_y - 1 do
        line size_x;
        row y;
      done;
      line size_x; Printf.printf "\n"
end

