class entity name start_x start_y col =
object (self)
  val mutable x:int = start_x
  val mutable y:int = start_y
  val mutable color = col

  (* return position in form of a pair of int *)
  method get_position = (x, y)

  (* set position to nx ny *)
  method move_to nx ny =
    begin
      x <- nx;
      y <- ny;
    end

  (* move using a delta (use move_to) *)
  method move_by dx dy =
    self#move_to (x+dx) (y+dy)

  (* get and set color *)
  method get_color = color
  method set_color c = color <- c

  (* get name *)
  method get_name = name

  (* return a string representing the entity *)
  method to_string =
    Printf.sprintf "%s[%s](%d,%d)" name color x y
end

