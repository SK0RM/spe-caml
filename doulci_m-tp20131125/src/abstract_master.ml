class virtual ['a] abstract_master (turns:int) (board:'a) =
object (self)

  val mutable current = 0

  (* Actor management *)
  val actors = new chain_actor
  method register_actor a = actors#register a

  (* virtual display *)
  method virtual display : unit

  (* virtual init *)
  method virtual init_board : unit

  method next_turn = 
    begin
      current <- current + 1;

      if current > turns then (raise The_End);
      board#iterate actors;
      self#display;
    end
end

