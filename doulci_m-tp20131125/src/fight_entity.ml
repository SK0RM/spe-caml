class ['ae] fight_entity name start_x start_y col board strength resistance =
object (self)
  inherit move_entity name start_x start_y col board as m_ent

  (* get strength and resistance *)
  method get_strength:int = strength
  method get_resistance:int = resistance

  (* defense against ae, another-entity *)
  method defense (ae:'ae) =
    if (resistance < ae#get_strength) then
      self#set_color(ae#get_color)

  (* atack *)
  method attack (ae:'ae) =
    ae#defense(self)

  (* Override move_to *)
  method move_to nx ny =
    try
       self#attack(board#get_entity nx ny) 
    with
      | Cell_empty(nx, ny) -> m_ent#move_to nx ny
      (*| e -> raise (e)*)
end

