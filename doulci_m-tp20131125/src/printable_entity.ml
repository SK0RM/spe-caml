class printable_entity name start_x start_y col =
object (self)
  (* inherit from entity *)
  inherit entity name start_x start_y col

  (* add a printer *)
  val printer = new print_helper

  (* Use printer to print *)
  method print = printer#print

  initializer
    (* Register to the printer *)
    printer#register self
end

