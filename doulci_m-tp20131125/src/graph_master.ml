class ['a] graphic_master turns board size_x size_y colors =
object (self)
  inherit ['a] team_master turns board size_x size_y

  val tab = Hashtbl.create 11

  initializer
    List.iter (fun (col,ch) -> Hashtbl.add tab col ch) colors;
            Graphics.open_graph (Printf.sprintf " %dx%d" size_x size_y);

  method display =
    begin
      Graphics.clear_graph ();
      for x = 0 to size_x - 1 do
        for y = 0 to size_y - 1 do
          try
            Graphics.set_color
              (Hashtbl.find tab ((board#get_entity x y)#get_color));
            Graphics.plot x y
          with 
            | _ -> ()
        done
      done;
      (* Wait for a Key *)
      ignore (Graphics.wait_next_event [Graphics.Key_pressed]);
    end
end

