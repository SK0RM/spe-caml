class ['a] random_move_actor range_x range_y range_dist =
object (self)
  inherit ['a] abstract_actor

  method treat e =
    let sign = if ((Random.int 42) mod 2 = 0) then 1 else -1 in
    let (dist, coef) = (Random.int range_dist, Random.float 1.) in
    let (cx, cy) = (coef, 1.0 -. coef) in
    let dist2 = (float (dist * dist)) in
    let d = sqrt(dist2 /. (cx *. cx +. cy *. cy)) in
    let (dx, dy) = (sign * int_of_float(cx *. d), sign * int_of_float(cy *. d)) in
    let (x, y) = e#get_position in
    let nx = max 0 (min (range_x - 1) (dx + x))
    and ny = max 0 (min (range_y - 1) (dy + y)) in
      e#move_to nx ny
end

