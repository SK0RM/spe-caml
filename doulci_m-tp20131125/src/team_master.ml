class virtual ['a] team_master turns board size_x size_y =
object (self)
  inherit ['a] abstract_master turns board

  val red_team = new team board "red" 60 
  val blue_team = new team board "blue" 50
  val green_team = new team board "green" 40
  val teams = Array.create 3 (Obj.magic None)

  initializer
    teams.(0) <- red_team;
              teams.(1) <- green_team;
              teams.(2) <- blue_team;

  method init_board =
    for x = 0 to size_x - 1 do
      for y = 0 to size_y - 1 do
        if (Random.int 5 = 0) then
          board#put_entity x y ( (teams.(Random.int 3))#new_member x y );
      done
    done
end

