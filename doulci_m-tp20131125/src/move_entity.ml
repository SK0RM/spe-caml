class move_entity name start_x start_y col (board:'a) =
object (self:'self)
  inherit entity name start_x start_y col as ent

         constraint 'a = 'self #virtual_board

  method move_to nx ny =
    begin
      board#move_entity x y nx ny;
      ent#move_to nx ny;
    end

  initializer
    board#put_entity start_x start_y self
end

