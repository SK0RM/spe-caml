class ['a, 'b] iterable_board (board:'a virtual_board) =
object (self)

  inherit ['a] virtual_board

  (* Forwarded methods *)
  method get_entity x y = board#get_entity x y
  method move_entity old_x old_y x y = board#move_entity old_x old_y x y

  (* add some store for entities, like a list *)
  val mutable entities = []

  (* forward put_entity to the board
   * and then (if nothing goes wrong)
   * add it to the store
   * *)
  method put_entity x y ent =
    try
      board#put_entity x y ent;
      entities <- ent::entities;
    with
        e -> () (*raise (e)*)

  (* iterate over each entity e and call actor#treat e *) 
  method iterate (actor:'b) =
    let rec iter = function
      | [] -> ()
      | e::l -> actor#treat e; iter l
    in iter entities
end

