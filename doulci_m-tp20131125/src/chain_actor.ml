class ['a, 'b] chain_actor =
object
  inherit ['a] abstract_actor

    constraint 'b = 'a #abstract_actor

  val actors = Queue.create ()

  method register (a:'b) =
    Queue.push a actors

  method treat e =
    Queue.iter (fun a -> a#treat e) actors
end

