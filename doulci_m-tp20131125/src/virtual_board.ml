class virtual ['a] virtual_board =
object
  method virtual put_entity: int -> int -> 'a -> unit
  method virtual get_entity: int -> int -> 'a
  method virtual move_entity: int -> int -> int -> int -> unit
end

