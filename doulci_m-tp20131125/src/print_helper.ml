class ['obj] print_helper =
object (self)
  (* JEDI MODE: you don't see the next line *)
  val mutable obj:'obj = Obj.magic None
  method register o = obj <- o
  method print =
    Format.printf "@[<b 2>Entity:@;@[<h>%s@]@]@," obj#to_string
end

