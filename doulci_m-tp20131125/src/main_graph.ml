let main_graph (w, h, l) =
    begin
        let (dX, dY) = (w, h) in
        let board = new iterable_board (new board dX dY) in
        let master = new graphic_master 10 board dX dY
        [("red",Graphics.red);("green",Graphics.green);("blue",Graphics.blue)]
        in
        master#register_actor (new random_move_actor dX dY l);
        master#init_board;
        master#display;
        until_the_end master;
        exit 0
    end

