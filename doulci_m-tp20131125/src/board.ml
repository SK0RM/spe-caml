(* 'a: will be the type of element on the board *)
class ['a] board size_x size_y =
object (self)
  inherit ['a] virtual_board
  val tab = Array.make_matrix size_x size_y None

  (* put an entity at (x,y) *)
  (* raise Cell_not_empty if needed *)
  method put_entity x y (ent:'a) =
    begin
      match (tab.(x).(y)) with
        | None -> tab.(x).(y) <- Some(ent)
        | Some(_) -> raise(Cell_not_empty(x, y))
    end

  (* return the entity at (x,y) *)
  (* raise Cell_empty if needed *)
  method get_entity x y =
    match tab.(x).(y) with
      | None -> raise(Cell_empty(x, y))
      | Some(e) -> e

  (* move some entity from pos to pos *)
  method move_entity old_x old_y x y =
    begin
      self#put_entity x y (self#get_entity old_x old_y);
      (* Thanks to exception, we only get there if everything is fine *)
      tab.(old_x).(old_y) <- None;
    end
end

