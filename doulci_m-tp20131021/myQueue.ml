type 'a cell = {
  content : 'a;
  mutable next : 'a cell;
}

type 'a queue = {
  mutable size : int;
  mutable cell : 'a cell option;
}
                   
let create () = {
  size = 0;
  cell = None;
}
(*
type 'a option =
  | None
  | Some of 'a*)

(* val is_empty : 'a queue -> bool *)
(* let is_empty q = (q.size = 0) *)
let is_empty q = (q.cell = None)

(* val length : 'a queue -> int *)
let length q = (q.size)

(* val push : 'a queue -> 'a -> unit *)
let push q x = 
    begin
        q.size <- q.size + 1;
        match (q.cell) with
        | None -> (* The queue is empty *)
                let rec c = { content = x ; next = c } in 
                q.cell <- Some(c);
        | Some(cell) -> (* The queue is not empty *)
                let c = { content = x ; next = cell.next } in
                cell.next <- c;
                q.cell <- Some(cell.next);
    end

(* val take : 'a queue -> 'a *)
(* 
 * // precondition: q.size > 0
 * take(q) {
 *   res = q.cell.next.content;
 *     q.size <- q.size - 1;
 *       if q.size = 0 then {
 *           q.cell <- empty cell;
 *             } else {
 *                 q.cell.next <- q.cell.next.next;
 *                   }
 *                     return res;
 *                     }
 *)
let take q = true 
