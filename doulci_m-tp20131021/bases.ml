type 'a cell = {
    content : 'a;
    mutable next : 'a cell;
}

(* val build_circular : 'a list -> 'a cell *)
(*let rec build_circular = function ->
    | [] -> Failwith "Carotte"
    | e::l -> build_rec { }
    let rec build_rec head l1 = match l1 with
    | [] -> Failwith "Liste vide"
    | e::[] -> let rec l1_ = { content = e ; next = head }; l1_
    | e::l -> { content = e ; next = build_rec l }
    in build_rec myList d
*)

let rec build_rec head l1 = match l1 with
    | [] -> head
    | e::l -> { content = e ; next = build_rec head l }

let build_circular l1 = match l1 with
    | [] -> failwith "CAROTTE !!"
    | e::l -> 
            let rec head = { content = e ; next = build_rec head l} in head
