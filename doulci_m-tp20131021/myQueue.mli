(* Interface for module MyQueue *)
                
(* Opaque Abstract Type *)
type 'a queue
                
(* Operations *)
val create : unit -> 'a queue
val is_empty : 'a queue -> bool
val length : 'a queue -> int
val push : 'a queue -> 'a -> unit
val take : 'a queue -> 'a
